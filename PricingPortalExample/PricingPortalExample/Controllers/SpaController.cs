﻿using Microsoft.AspNetCore.Mvc;
using PricingPortalExample.Data;
using PricingPortalExample.Models;
using PricingPortalExample.Models.ViewModels;

namespace PricingPortalExample.Controllers
{
    public class SpaController : Controller
    {
        private ISpaRepository repository;

        public SpaController(ISpaRepository repository)
        {
            this.repository = repository;
        }

        public ViewResult Create()
        {
            return View(new SpaViewModel());
        }

        [HttpPost]
        public ActionResult Create(SpaViewModel spa)
        {
            if (ModelState.IsValid)
            {
                // code to save for real repo goes here...
                return RedirectToAction("Index", "Home");
            }

            return View(spa);
        }

        //[HttpPost]
        //public ActionResult Create(Spa spa)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        // code to save for real repo goes here...
        //        return RedirectToAction("Index", "Home");
        //    }

        //    return View();
        //}
    }
}
