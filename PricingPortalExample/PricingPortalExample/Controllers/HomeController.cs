﻿using Microsoft.AspNetCore.Mvc;
using PricingPortalExample.Data;

namespace PricingPortalExample.Controllers
{
    public class HomeController : Controller
    {
        private ISpaRepository repository;

        public HomeController(ISpaRepository repository)
        {
            this.repository = repository;
        }

        public ViewResult Index()
        {
            return View();
        }
    }
}