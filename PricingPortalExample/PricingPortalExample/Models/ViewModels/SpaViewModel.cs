﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace PricingPortalExample.Models.ViewModels
{
    public class SpaViewModel
    {
        public Spa Spa { get; set; }

        public IEnumerable<SelectListItem> Countries { get; }

        public SpaViewModel()
        {
            Spa = new Spa();

            Countries = new List<SelectListItem>
            {
                new SelectListItem {Text = "Choose an option", Value = string.Empty},
                new SelectListItem {Text = "United Kingdom", Value = "0"},
                new SelectListItem {Text = "Austria", Value = "1"},
                new SelectListItem {Text = "Netherlands", Value = "2"}
            };
        }
    }
}
