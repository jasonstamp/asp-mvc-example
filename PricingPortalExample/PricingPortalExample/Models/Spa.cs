﻿using System.ComponentModel.DataAnnotations;

namespace PricingPortalExample.Models
{
    public class Spa
    {
        public int ID { get; set; }

        [Required]
        public string Agreement { get; set; }

        [Required]
        public int Country { get; set; }

        [Required]
        [StringLength(15)]
        public string ProjectDescription { get; set; }
    }
}
