﻿using System.Linq;
using PricingPortalExample.Models;

namespace PricingPortalExample.Data
{
    public interface ISpaRepository
    {
        IQueryable<Spa> Spas { get; }
    }
}
