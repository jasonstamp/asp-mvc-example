﻿using System.Collections.Generic;
using System.Linq;
using PricingPortalExample.Models;

namespace PricingPortalExample.Data
{
    public class FakeSpaRepository : ISpaRepository
    {
        public IQueryable<Spa> Spas => new List<Spa>
        {
            new Spa {ID = 1, Agreement = "4000406458", Country = 0, ProjectDescription = "Test"}
        }.AsQueryable();
    }
}
